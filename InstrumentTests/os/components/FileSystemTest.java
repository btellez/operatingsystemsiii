package os.components;

import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import os.Main;
import os.components.Disk.Block;
import os.components.Disk.Meta;
import os.components.Disk.OpenFileTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FileSystemTest {

    FileSystem fs;

    @Before
    public void setup() {
        Main.LOGGING = false;
        fs = new FileSystem();
    }

    @After
    public void teardown() {
        fs = null;
    }


    //**********************************************************//
    //                  Begin Tests FS                          //
    //**********************************************************//
    @Test
    public void testDescriptor() {
        Meta m = new Meta();
        Meta.Descriptor des = new Meta.Descriptor();
        assertEquals(0, des.getLength());

        // Test writing and reading a value.
        for (int i = 1; i < 4; i++) {
            int val1 = (new Random()).nextInt(57);
            int val2 = (new Random()).nextInt(64 * 3);

            des.setBlock(i, val1);
            assertEquals(val1, des.getBlockValue(i));

            des.setLength(val2);
            assertEquals(val2, des.getLength());
        }
    }


    @Test
    public void testDisk() {
        FileSystem.DiskInterface disk = new LogicalDisk();
        assertEquals(new Block(), disk.read_block(0));
    }

    //**********************************************************//
    //                  Begin Tests FS                          //
    //**********************************************************//
    @Test
    public void createFiles() {
        String[] files = createSeriesOfFiles();     // Create the series of files
        assertArrayEquals(files, fs.directory());
    }

    @Test
    public void deleteFile() {
        String[] files = createSeriesOfFiles();

        // Delete a file:
        fs.create("bad");
        fs.destroy("bad");
        assertArrayEquals(files, fs.directory());
    }

    @Test
    public void createMaxFiles() {
        List<String> files = new ArrayList<>();
        for (int i = 0; i < 17; i++) {
            String s = "f" + i;
            if (i < 16)
                files.add(s);
            fs.create(s);
        }
        assertArrayEquals(files.toArray(new String[files.size()]), fs.directory());
    }

    @Test
    public void openFile() {
        int[] openIndex = new int[3];
        String[] openFiles = new String[3];
        String[] files = createSeriesOfFiles();

        openFiles[0] = files[0];
        openFiles[1] = files[1];
        openFiles[2] = files[2];

        int index = 0;
        for (String file : openFiles) {
            openIndex[index] = fs.open(file);
            index++;
        }

        // try to open additinal files.
        assertEquals(-1, fs.open(files[3]));
        assertEquals(-1, fs.open(files[4]));

        assertEquals(openFiles.length + 1, fs.oft.size());

    }

    @Test
    public void closeFile() {
        String[] files = createSeriesOfFiles();

        assertFalse(fs.close(0)); // Can't close directory
        assertFalse(fs.close(1)); // No File open.

        int openIndex = fs.open(files[0]);
        assertTrue(fs.close(openIndex));
    }

    @Test
    public void readWriteBlock() {
        String[] files = createSeriesOfFiles();
        int[] opened = new int[3];
        opened[0] = fs.open(files[0]);
        opened[1] = fs.open(files[1]);
        opened[2] = fs.open(files[2]);

        // Write Some information.
        assertEquals(20, fs.write(opened[0], 'x', 20));
        assertEquals(20, fs.write(opened[1], 'y', 20));
        assertEquals(20, fs.write(opened[2], 'z', 20));


        String xs = "";
        String ys = "";
        String zs = "";
        for (int i = 0; i < 20; i++) {
            xs += "x";
            ys += "y";
            zs += "z";
        }

        fs.seek(opened[0], 0);
        assertEquals(xs, fs.read(opened[0], 20));
        fs.seek(opened[1], 0);
        assertEquals(ys, fs.read(opened[1], 20));
        fs.seek(opened[2], 0);
        assertEquals(zs, fs.read(opened[2], 20));

        // Read More than in Buffer.
        fs.seek(opened[0], 0);
        assertEquals(xs, fs.read(opened[0], 30));
        fs.seek(opened[1], 0);
        assertEquals(ys, fs.read(opened[1], 30));
        fs.seek(opened[2], 0);
        assertEquals(zs, fs.read(opened[2], 30));
    }

    @Test
    public void readWriteBlockLongExample() {
        String[] files = createSeriesOfFiles();
        int[] opened = new int[3];
        opened[0] = fs.open(files[0]);
        opened[1] = fs.open(files[1]);
        opened[2] = fs.open(files[2]);

        // Write Some information.
        assertEquals(65, fs.write(opened[0], 'x', 65));
        assertEquals(120, fs.write(opened[1], 'y', 120));
        assertEquals(160, fs.write(opened[2], 'z', 160));


        String xs = "";
        String ys = "";
        String zs = "";
        for (int i = 0; i < 65; i++) {
            xs += "x";
        }

        for (int i = 0; i < 120; i++) {
            ys += "y";
        }

        for (int i = 0; i < 160; i++) {
            zs += "z";
        }

        fs.seek(opened[0], 0);
        assertEquals(xs, fs.read(opened[0], 65));
        fs.seek(opened[1], 0);
        assertEquals(ys, fs.read(opened[1], 120));
        fs.seek(opened[2], 0);
        assertEquals(zs, fs.read(opened[2], 160));

        // Read More than in Buffer.
        fs.seek(opened[0], 0);
        assertEquals(xs, fs.read(opened[0], 70));
        fs.seek(opened[1], 0);
        assertEquals(ys, fs.read(opened[1], 125));
        fs.seek(opened[2], 0);
        assertEquals(zs, fs.read(opened[2], 165));
    }


    @Test
    public void seekToPosition() {
        String[] files = createSeriesOfFiles();
        int index = fs.open(files[0]);
        fs.write(index, 'y', 64);

        assertEquals(fs.seek(index, 5), 5);
    }

    @Test
    public void directory() {
        String[] files = createSeriesOfFiles();
        assertArrayEquals(files, fs.directory());
    }

    @Test
    public void saveAndLoadFromDisk() {
        String[] files = createSeriesOfFiles();
        int[] opened = new int[3];
        // open some files.
        opened[0] = fs.open(files[0]);
        opened[1] = fs.open(files[1]);
        opened[2] = fs.open(files[2]);

        fs.write(opened[0], 'x', 20);
        fs.write(opened[1], 'o', 40);
        fs.write(opened[2], 's', 30);

        fs.seek(opened[0],0);
        fs.read(opened[0], 20);

        fs.seek(opened[1],0);
        fs.read(opened[1], 20);

        fs.save();
        fs = null;

        fs = new FileSystem();
        fs.init();
        fs.save();

        assertArrayEquals(files, fs.directory()); // The files should still be in the same directory.
        assertEquals(1, fs.oft.size()); // Only directory should be open.

    }

    @Test
    public void oftTest() {
        OpenFileTable oft = new OpenFileTable();
        oft.add(new Block(), 0, 0, 0, 1);
        oft.add(new Block(), 0, 0, 0, 2);
        oft.add(new Block(), 0, 0, 0, 3);
        oft.add(new Block(), 0, 0, 0, 4);
        assertEquals(4, oft.size());
    }

    private String[] createSeriesOfFiles() {
        String[] files = new String[]{"xxx","yyy","zzz", "uu", "aa", "bb"};
        for (String f: files)
            fs.create(f);
        return files;
    }
}
