# Operating Systems: Project 3.
Simulation of a filesystem in an operating system.
## Partners:

- Bladymir Tellez
  - \#76123163
  - tellezb@uci.edu
- Calvin Liu #
  - \#34749291
  - calvinl1@uci.edu

To Run the project:

```
$  cd build
$  make run
$  make diff
```

Note: The output file may have extra unwanted characters that may make
`diff` not function correctly.
