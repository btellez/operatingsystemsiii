package os.components.Disk;

import java.io.Serializable;

/**
 * Represents a single block on disk.
 */
public class Block {
    private byte[] mContents;
    private byte mUsed;

    public Block() {
        this(new byte[64], (byte) 0);
    }

    public Block(byte[] bytes, byte used) {
        mContents = bytes;
        mUsed = used;
    }

    /**
     * Returns the contents of the provided index.
     * @param index
     * @return
     */
    public char get(int index) {
        if (mContents[index] > 0)
            return (char) mContents[index];
        else
            return 0;
    }

    /**
     * Set the contents at the given index.
     * @param index
     * @param content
     */
    public void set(int index, char content) {
        if (index >= mContents.length)
            return; // Can Not Set Contents Out of Bounds.
        mContents[index] = (byte) content;
        if (index > mUsed)
            mUsed++;

    }

    public Block clone() {
        return new Block(mContents.clone(), mUsed);
    }

    @Override
    public String toString() {
        String nl = "\n";
        StringBuilder sb = new StringBuilder();
//        Descriptor stores the length. Skip storing length here.
//        sb.append(mUsed).append(nl); // The Quantity Actually Used.
        for (byte b : mContents) {
            sb.append(b).append(nl); // The value of the bytes.
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (obj instanceof Block) {
            boolean result = true;
            for (int i = 0; i<mContents.length; i++)
                result &= mContents[i] == ((Block) obj).mContents[i];
            return result;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return (mContents.length * 100) + mUsed;
    }
}
