package os.components.Disk;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class OpenFileTable {
    private TableRow[] mFileTable;
    private Stack<Integer> available = new Stack<>();

    public OpenFileTable() {
        mFileTable = new TableRow[4];
        for (int i = 3; i > 0; i--)
            available.push(i);
        available.push(0); // Directory will take first.
    }

    public int[] getOpenIndecies() {
        List<Integer> open = new ArrayList<>();
        for (int i = 1; i < mFileTable.length; i++) {
            if (mFileTable[i] != null)
                open.add(i);
        }

        int[] val = new int[open.size()];
        int i = 0;
        for (int v : open)
            val[i++] = v;

        return val;
    }

    public Block getBuffer(int index) {
        return mFileTable[index].buffer;
    }

    public void setBuffer(int index, Block block){
        mFileTable[index].buffer = block;
    }

    public int add(Block b, int pos, int length, int des_index, int block_index) {
        TableRow tr = new TableRow();
        tr.buffer = b;
        tr.position = pos;
        tr.length = length;
        tr.descriptor = des_index;
        tr.block_index = block_index;

        int slot = available.pop();
        mFileTable[slot] = tr;
        return slot;
    }

    public boolean remove(int index) {
        if (index == 0 || index > mFileTable.length - 1)
            return false;
        mFileTable[index] = null;
        available.push(index);
        return true;
    }

    public int size() {
        return 1 + 3 - available.size();
    }

    public int getPosition(int index) {
        return mFileTable[index].position;
    }

    public void setPosition(int index, int position){
        mFileTable[index].position = position;
    }

    public int getLength(int index) {
        return mFileTable[index].length;
    }

    public void setLength(int index, int length){
        mFileTable[index].length = length;
    }

    public int getDescriptorIndex(int index) {
        return mFileTable[index].descriptor;
    }

    public int getBlockIndex(int index) {
        return mFileTable[index].block_index;
    }

    public boolean hasIndex(int index) {
        if (index >= mFileTable.length)
            return false;
        if (mFileTable[index] == null)
            return false;
        return true;
    }

    public int indexOf(int descriptorOffset) {
        for (int i = 1; i< mFileTable.length; i++) {
            if (mFileTable[i] != null && mFileTable[i].descriptor == descriptorOffset)
                return i;
        }
        return -1; // index not in OFT.
    }

    /**
     * Represents a single row of the OFT.
     */
    private class TableRow {
        /**
         * The current block in the buffer.
         */
        public Block buffer;

        /**
         * The current position in the file. Range: [0, 192)
         */
        public int position;

        /**
         * The length of the current file. [0, 192)
         */
        public int length;

        /**
         * The index of the descriptor
         */
        public int descriptor;

        /**
         * The index of the block in the buffer, on disk.
         */
        public int block_index;
    }
}
