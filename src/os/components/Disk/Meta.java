package os.components.Disk;

import os.components.FileSystem;

import java.util.Arrays;
import java.util.Stack;

/**
 * Represents the Disk's Meta Data
 */
public class Meta {

    private static long[] MASK;
    private static short MAX_INDEX = 56;
    private static byte LIMIT_DESCRIPTORS = 24;
    private Descriptor[] mDescriptors = new Descriptor[LIMIT_DESCRIPTORS];
    private int mUsed;
    private long bitmask;
    private Directory directory;
    private Descriptor nextAvailableDescriptor;
    private Stack<Integer> availableDescriptors = new Stack<>();

    public Meta() {
        // Fill the descriptor Array.
        for (int i = 0; i < LIMIT_DESCRIPTORS; i++) {
            mDescriptors[i] = new Descriptor();
        }

        for (int i = LIMIT_DESCRIPTORS - 1; i > 0; i--) {
            availableDescriptors.push(i); // Indecies 1 to Limit are avilable.
        }

        // Allocate the first block to the the Directory.
        allocateBlock(mDescriptors[0], 0);
        allocateBlock(mDescriptors[0], 1);
        allocateBlock(mDescriptors[0], 2);
        directory = new Directory(mDescriptors[0]);
        mUsed++;
    }


    /**
     * Returns the Block offset to start writing to.
     * @param file_name
     * @return
     */
    public int create(String file_name) {
        return directory.newFile(file_name);
    }

    public int destroy(String file_name) {
        return -1;
    }

    public Descriptor getDescriptor(int index) {
        return mDescriptors[index];
    }
    public int getDescriptorsAvailable() {
        return LIMIT_DESCRIPTORS - mUsed;
    }

    public int getLength(int descriptorIndex) {
        if (descriptorIndex >= mUsed)
            return -1; // Error.
        return mDescriptors[descriptorIndex].getLength();
    }

    public int getBlockOffset(int descriptorIndex, int blockIndex) {
        if (descriptorIndex >= mUsed || blockIndex > 2)
            return -1;
        return mDescriptors[descriptorIndex].getBlockValue(blockIndex);
    }

    public int getIndexFirstEmptySlot() {
        for (int i = 0; i < MAX_INDEX; i++) {
            if ((MASK[i] & bitmask) == 0)
                return i; // The i-th hols is empty.
        }
        return -1; // Filled Disk. No Holes.
    }

    public long getBitmask(){
        return bitmask;
    }

    public void setBitmask(long bm) {
        bitmask = bm;
    }

    private void allocateBlock(Descriptor desc, int blockIndex) {
        long setOccupied =((long) 1 << blockIndex);
        if ((bitmask & setOccupied) > 0)
            return; // Do Not Allocate

        desc.setBlock(0, blockIndex);   // Save index of the block in descriptor.
        bitmask |= setOccupied;         // Set Block Occupied in mask.
    }

    private void releaseBlock(int blockIndex) {
        long toRelease = ~((long) 1 << blockIndex);
        bitmask &= toRelease; // keeps 1's everywhere except the blockIndex provided.
    }

    /**
     * Returns the encoding of the Bitmap and File Descriptors, in order.
     * @return
     */
    public String getEncoded() {
        String nl = "\n";
        StringBuffer sb = new StringBuffer();
        sb.append(bitmask).append(nl);
        for (Descriptor d : mDescriptors)
            sb.append(d.toString()).append(nl);
        return sb.toString();
    }

    public void fromEncoding(String in) {

    }

    @Override
    public String toString() {
        return getEncoded();
    }

    /**
     * Initialize static variable values.
     */
    static {
        MASK = new long[64];
        for (int i = 0; i < 64; i++)
            MASK[i] = (long)1 << i;
    }

    public void makeAvailable(int descriptorOffset) {
        for (int i = 1; i <= 3; i++) {
            int blockNumber = mDescriptors[descriptorOffset].getBlockValue(i);
            if (blockNumber > 0)
                releaseBlock(blockNumber);
        }
        mDescriptors[descriptorOffset] = new Descriptor();
    }

    public int findAvailableBlock() {
        return getIndexFirstEmptySlot();
    }

    public void markAllocated(int blockIndex) {
        long setOccupied =((long) 1 << blockIndex);
        bitmask |= setOccupied;         // Set Block Occupied in mask.
    }

    public int getNextAvailableDescriptor() {
        return availableDescriptors.pop();
    }

    public void calculateUnusedDescriptors() {
        availableDescriptors.clear();
        for (int i = 0; i < mDescriptors.length; i++) {
            if (mDescriptors[i].getLength() == 0 && mDescriptors[i].getBlockValue(1) == 0)
                availableDescriptors.push(i);
        }
    }

    /**
     * Helper Methods for dealing with descriptors
     */
    public static class Descriptor {
        // 64 bit Descriptor
        // private final static int LENGTH_OFFSET = 48;
        // private final static int BLOCK_1_OFFSET = 32;
        // private final static int BLOCK_2_OFFSET = 16;
        // private final static int BLOCK_3_OFFSET = 0;

        private final static int BLOCK_SIZE = 16;
        private final static int BLOCK_COUNT = 4;

        private long blocks = 0;
        private int nextEmptyBlockPointer;

        public Descriptor() {
            this(0);
        }

        public Descriptor(long init) {
            blocks = init;
        }

        public Descriptor(int length) {
            setLength(length);
        }

        /**
         * Set the Length of the descriptor.
         * @param length
         */
        public void setLength(int length) {
            setBlock(0, length);
        }

        public short getLength() {
            return getBlockValue(0);
        }

        /**
         * Set the block at the given index to the provided value
         * @param index -- index in descripotr
         * @param value -- offset value for actual block.
         */
        public void setBlock(int index, int value){
            setValue(index, value);
        }

        public short getBlockValue(int index) {
            return (short) ((blocks & mask(index)) >> shiftAmount(index));
        }

        /**
         * Set the value of the block at the provided index.
         * Indexed left to right.
         *
         * @param index
         * @param value
         */
        private void setValue(int index, int value) {
            clearBlock(index);
            blocks |= ((long) ((short)value) << shiftAmount(index));
        }

        /**
         * Clear the value of the block at the provided index.
         * Indexed left to right.
         *
         * @param index
         */
        private void clearBlock(int index) {
            blocks = (blocks & ~mask(index));
        }

        /**
         * Calculate the shift amount for a block.
         * @param index
         * @return
         */
        private int shiftAmount(int index) {
            return (BLOCK_SIZE * index);
        }

        /**
         * Get the appropriate bit-mask.
         * @param index
         * @return
         */
        private long mask(int index) {
            long mask = 0;
            int shiftAmount = shiftAmount(index);
            for (int i = shiftAmount; i < shiftAmount + BLOCK_SIZE; i++)
                mask |= MASK[i]; // Set the bit
            return mask;
        }


        @Override
        public String toString() {
            return String.valueOf(blocks);
        }

        public void setContents(long bs) {
            blocks = bs;
        }

        public int blockIndexOfPosition(int position) {
            int blockPosition = (position / 64) + 1;
            return blockPosition;
        }

        public int getNextEmptyBlockPointer() {
            for (int i = 1; i <= 3; i++) {
                if (getBlockValue(i) == 0) // No Block Value Set. 0 is reserved for directory.
                    return i;
            }
            return -1; // All Blocks occupied.
        }
    }


    private class Directory {
        private Descriptor descriptor;
        private int fileCount;

        public Directory(Descriptor in_descriptor) {
            descriptor = in_descriptor;
            fileCount = 0;
        }

        public int newFile(String name) {
            int blockIndex = fileCount % 64;                            // Block to start inputing new file.
            fileCount += 4;
            return blockIndex;
        }

    }

    public interface BlockListener {
        public void allocateBlock(int index);
    }
}
