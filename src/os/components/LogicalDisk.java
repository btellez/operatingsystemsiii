package os.components;

import os.components.Disk.Block;
import os.components.Disk.Meta;

public class LogicalDisk implements FileSystem.DiskInterface {

    private static final int MAX_BLOCKS = 57; // 64 Blocks - 7 used for meta

    /**
     * Stores the actual data.
     */
    private Block[] disk = new Block[MAX_BLOCKS];

    /**
     * Maintains a mapping of the allocated blocks, their sizes, and descriptors.
     */
    private Meta meta = new Meta();

    public LogicalDisk() {
        for (int i = 0; i < MAX_BLOCKS; i++)
            disk[i] = new Block();
    }

    @Override
    public Block read_block(int i) {
        return disk[i].clone();
    }

    @Override
    public void write_block(int i, Block ptr) {
        disk[i] = ptr.clone();
    }

    @Override
    public Meta meta() {
        return meta;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(meta.toString());
        for (Block b : disk)
            sb.append(b);
        return sb.toString();
    }
}
