package os.components;

import os.Main;
import os.components.Disk.Block;
import os.components.Disk.Meta;
import os.components.Disk.OpenFileTable;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class FileSystem {

    DiskInterface disk;
    OpenFileTable oft;

    public FileSystem() {
        reset();
    }

    public void reset() {
        disk = new LogicalDisk();
        oft = new OpenFileTable();
        // Create the directory:::
        disk.meta().getDescriptor(0).setLength(192);
        disk.meta().getDescriptor(0).setBlock(1, 0);
        disk.meta().getDescriptor(0).setBlock(2, 1);
        disk.meta().getDescriptor(0).setBlock(3, 2);
        disk.meta().markAllocated(0);
        disk.meta().markAllocated(1);
        disk.meta().markAllocated(2);
        oft.add(disk.read_block(0), // Load Block to buffer
                0,                  // Starting position
                192,                // Length of the directory
                0,                  // Index of the descriptor
                0);                 // Index of the block on disk.
    }

    /**
     * Create the file in the file system with the given name
     *
     * @param file_name
     */
    public boolean create(String file_name) {
        file_name = file_name.trim();
        Main.Log("Creating: " + file_name);
        String[] dirFiles = directory();
        if (dirFiles.length == 16) {
            Main.Write("Error: File System Has Reached Maximum file creations.");
            return false;
        }
        if (Arrays.asList(dirFiles).contains(file_name)) {
            Main.Write(String.format("Error: file \"%s\" already exists.", file_name));
            return false;
        }

        int offset = disk.meta().create(file_name);                     // Update meta and get offset.
        if (offset >= 0) {
            Main.Write(String.format("file \"%s\" created", file_name));
            Block buffer = oft.getBuffer(0);
            char[] name = file_name.substring(0, Math.min(file_name.length(), 3)).toCharArray();
            for (int i = 0; i < 3; i++) {
                if (i < name.length)
                    buffer.set(offset, name[i]);
                else
                    buffer.set(offset, (char) 0);
                offset++;
            }
            buffer.set(offset, (char) -1); // No descriptor for the new file yet.
            disk.write_block(disk.meta().getDescriptor(oft.getDescriptorIndex(0)).getBlockValue(1), buffer); // Save new info to disk.
            return true;
        } else {
            Main.Write(String.format("Error: file \"%s\" creation failed", file_name));
            return false;
        }
    }

    /**
     * Destroy the file in the file system with the given name
     *
     * @param file_name
     */
    public boolean destroy(String file_name) {
        Main.Log("Destroying: " + file_name);
        String[] directory = directory();
        if (Arrays.asList(directory()).contains(file_name)) {
            int targetOffset = getFileOffset(directory, file_name);
            int descriptorOffset = oft.getBuffer(0).get(((targetOffset * 4) + 4) - 1);
            if (oft.indexOf(descriptorOffset) > 0) {
                close(oft.indexOf(descriptorOffset), false);
            }
            int originalOffset = getFileOffset(directory, directory[directory.length - 1]);
            markDescriptorAvailable(descriptorOffset);
            moveEntry(targetOffset, originalOffset);
            Main.Write(String.format("file \"%s\" destroyed", file_name));
            return true;
        } else {
            Main.Write(String.format("Error: file \"%s\" destroyed failed", file_name));
            return false;
        }
    }

    private void moveEntry(int targetOffset, int originalOffset) {
        Block buffer = oft.getBuffer(0);
        if (targetOffset == originalOffset) {
            for (int i = 0; i < 4; i++) {
                buffer.set(4 * originalOffset + i, (char) 0);                    // Clear old Contents.
            }
        } else {
            for (int i = 0; i < 4; i++) {
                buffer.set(targetOffset + i, buffer.get(originalOffset + i));   // Move Contents.
                buffer.set(originalOffset + i, (char) 0);                       // Clear old Contents.
            }
        }


        disk.write_block(0, buffer);
    }

    private void markDescriptorAvailable(int descriptorOffset) {
        disk.meta().makeAvailable(descriptorOffset);
    }

    /**
     * Opens a file in the file system.
     *
     * @param file_name
     * @return index from open file table
     */
    public int open(String file_name) {
        Main.Log("Opening: " + file_name);

        if (oft.size() == 4) {
            Main.Write("Error: Can not Open additional files. Max number of files have been opened.");
            return -1;
        }

        int index = -1;
        String[] directory = directory();
        if (Arrays.asList(directory()).contains(file_name)) {
            int fileOffset = getFileOffset(directory, file_name);
            int descriptorOffset = oft.getBuffer(0).get(((fileOffset * 4) + 4) - 1);
            int block_index;
            if (descriptorOffset > 0) { // Make sure we have a valid non-directory descriptor.
                Meta.Descriptor d = disk.meta().getDescriptor(descriptorOffset);
                block_index = d.getBlockValue(1);
                index = oft.add(disk.read_block(block_index), 0, d.getLength(), descriptorOffset, block_index);  // Block has no length
            } else {
                descriptorOffset = disk.meta().getNextAvailableDescriptor();
                Meta.Descriptor d = disk.meta().getDescriptor(descriptorOffset);
                block_index = disk.meta().findAvailableBlock();
                disk.meta().markAllocated(block_index);
                d.setBlock(1, block_index);
                index = oft.add(disk.read_block(block_index), 0, 0, descriptorOffset, block_index);                                      // Block has no length.
                updateDirectory(fileOffset, descriptorOffset); // set the directory offset into the buffer
                disk.write_block(oft.getBlockIndex(0), oft.getBuffer(0)); // Write the new file descriptor options to disk.
            }
            Main.Write(String.format("file %s opened, index=%d", file_name, index));
        } else {
            Main.Write(String.format("Error: file %s not found", file_name));
        }
        return index;
    }

    private void updateDirectory(int fileOffset, int descriptorIndex) {
        oft.getBuffer(0).set(((fileOffset * 4) + 4) - 1, (char) descriptorIndex);
    }

    private int getFileOffset(String[] directory, String file_name) {
        for (int i = 0; i < directory.length; i++) {
            if (directory[i].equalsIgnoreCase(file_name))
                return i;
        }
        return -1;
    }

    private int getDescriptorDirectory() {
        Block buffer = oft.getBuffer(0);            // Get Directory Buffer
        return 0;
    }


    public boolean close(int index) {
        return close(index, true);
    }

    /**
     * Close the file at the given index of the open file table.
     *
     * @param index
     */
    public boolean close(int index, boolean enableOutput) {
        Main.Log("Closing Index: " + index);
        boolean result = false;
        if (index > 0 && oft.hasIndex(index)) {
            // Write Buffer to disk, and remove from open file index.
            disk.write_block(oft.getDescriptorIndex(index), oft.getBuffer(index));
            oft.remove(index);
            if (enableOutput)
                Main.Write(String.format("file %d closed", index));
            result = true;
        } else {
            if (enableOutput)
                Main.Write(String.format("Error: Can Not Close File at Index: %d", index));
            result = false;
        }
        return result;
    }

    /**
     * @param index - Open File Table file index.
     * @param count - Quantity of bytes to read.
     * @return Number of bytes read.
     */
    public String read(int index, int count) {
        Main.Log("Reading (" + index + "," + count + ")");
        int actualRead = -1;
        String output = "";

        Block buffer = oft.getBuffer(index);       // The Current Buffer
        int position = oft.getPosition(index);     // The current Position
        int length = oft.getLength(index);         // Length of the file.
        boolean overRun = false;

        int block_index = oft.getBlockIndex(index);     // Get the block index of the current buffered block
        Meta.Descriptor descriptor = disk.meta().getDescriptor(oft.getDescriptorIndex(index)); // get the descriptor.

        if (buffer != null) {
            actualRead = 0;
            for (int i = 0; i < count; i++, position++, actualRead++) {
                int testBlockIndex = descriptor.getBlockValue(descriptor.blockIndexOfPosition(position));
                if (position < length && position < 192) {
                    if (block_index != testBlockIndex) {
                        block_index = testBlockIndex; // get index of new block
                        buffer = disk.read_block(block_index);
                        oft.setBuffer(index, buffer);
                    }
                    output += buffer.get(position % 64);
                } else {
                    overRun = true;
                    oft.setPosition(index, 0);
                    break;
                }
            }
            oft.setPosition(index, position);
            if (overRun)
                Main.Write("Error: Tried To Read Amount Larger than file Length, setting position to start of file.");
            Main.Write(String.format("%d bytes read: %s", actualRead, output));
        } else {
            Main.Write(String.format("Error: Can Read From Index: %d", index));
        }

        return output;
    }

    /**
     * @param index - Open File Table file index.
     * @param input - Data Structure to copy data from.
     * @param count - Quantity of bytes to write.
     * @return Number of bytes written.
     */
    public int write(int index, char input, int count) {
        Main.Log("Writing (" + index + "," + count + ")");
        int actualWritten = -1;

        Block buffer = oft.getBuffer(index);        // Get the current buffer.
        int block_index = oft.getBlockIndex(index);     // Get the block index of the current buffered block
        Meta.Descriptor descriptor = disk.meta().getDescriptor(oft.getDescriptorIndex(index)); // get the descriptor.

        if (buffer != null) {
            actualWritten = 0;          // Track how much we actually write.
            int position = oft.getPosition(index);   // get the current position.
            int length = oft.getLength(index);       // get the current length.
            for (int i = 0; i < count; i++, position++, actualWritten++) {
                int testBlockIndex = descriptor.getBlockValue(descriptor.blockIndexOfPosition(position));
                if (testBlockIndex == 0) { // We do not have another block to write to.
                    testBlockIndex = disk.meta().findAvailableBlock();
                    int nextBlock = descriptor.getNextEmptyBlockPointer();
                    disk.meta().markAllocated(testBlockIndex);
                    if (nextBlock == -1) {
                        seek(index, 0, false);
                        Main.Write(String.format("Error: Max Length Reached. Setting position=0. %d bytes written", actualWritten));
                        break;
                    }
                    descriptor.setBlock(nextBlock, testBlockIndex);
                }
                if (testBlockIndex != block_index) { // Reached end of buffer.
                    disk.write_block(block_index, buffer); // Write current block to disk
                    block_index = testBlockIndex; // get index of new block
                    buffer = disk.read_block(block_index);
                    oft.setBuffer(index, buffer);
                }
                buffer.set(position % 64, input); // Write into the same buffer.
            }

            oft.setPosition(index, position);
            oft.setLength(index, Math.max(position, length));
            descriptor.setLength(Math.max(position, length));
            disk.write_block(block_index, buffer);

            Main.Write(String.format("%d bytes written", actualWritten));
        } else {
            Main.Write(String.format("Error: Can Write to Index: %d", index));
        }
        return actualWritten;
    }

    public int seek(int index, int position) {
        return seek(index, position, true);
    }

    /**
     * Set the cursor to a specified position of the file
     *
     * @param index    - Open File Table file index.
     * @param position - Byte to set the cursor to.
     * @return
     */
    public int seek(int index, int position, boolean enableOutput) {
        Main.Log("Seeking (" + index + "," + position + ")");
        Meta.Descriptor descriptor = disk.meta().getDescriptor(oft.getDescriptorIndex(index));
        if (enableOutput) {
            if (position >= 0 && position < oft.getLength(index) && position < 192) {
                oft.setPosition(index, position);
                int blockIndex = descriptor.blockIndexOfPosition(position); // Which block to look at
                int blockOffset = descriptor.getBlockValue(blockIndex);
                oft.setBuffer(index, disk.read_block(blockOffset));
                Main.Write(String.format("current position is: %d", position));
            } else {
                int blockOffset = descriptor.getBlockValue(1);
                oft.setBuffer(index, disk.read_block(blockOffset));
                oft.setPosition(index, 0);
                Main.Write(String.format("Error: Cannot seek past end of file. Position set to 0")); // Off end. Set=0.
            }
        }
        return position;
    }

    public String[] directory() {
        return directory(false);
    }

    /**
     * List the files that are on the file system.
     *
     * @return
     */
    public String[] directory(boolean enableOutput) {
        Main.Log("Listing Directory");
        seek(0, 0, false);                         // Set directory to start of the file.
        Block buffer = oft.getBuffer(0);    // Get the buffer
        List<String> files = new ArrayList<>(); // Track List of files.
        for (int i = 0; i < 64; i++) {
//            if (i == 64)
//                buffer = disk.read_block(disk.meta().getDescriptor(0).getBlockValue(2));
//            else if (i == 128)
//                buffer = disk.read_block(disk.meta().getDescriptor(0).getBlockValue(3));
            char[] name = new char[3];
            for (int j = 0; j < 3; j++) {
                name[j] = buffer.get(i); // %64
                i++;
            }
            if (!allZero(name)) {
                files.add(String.valueOf(trimZero(name)));
            }
        }

        String str = "";
        if (files.size() > 0) {
            for (String s : files)
                str += s + ", ";
//            str = str.length() > 2 ? str.substring(0, str.length() - 2) : str;
        } else {
            str = "No Files on Disk";
        }

        if (enableOutput)
            Main.Write(str);

        return trimNull(files.toArray(new String[files.size()]));
    }


    /**
     * Initialize the logical disk with the default file.
     */
    public void init() {
        init(DEFAULT_SAVE_FILE);
    }

    /**
     * Initialize the logical disk from the file specified.
     *
     * @param file_name
     */
    public void init(String file_name) {
        Main.Log("Initializing disk with the \"" + file_name + "\" file.");
        File file = new File(file_name);
        if (!file.exists()) {
            // Leave disk in the state it is in.
            if (!file_name.equals(DEFAULT_SAVE_FILE)) {
                reset();
                Main.Write(file_name+" does not exist. Initializing disk instead.");
            }
            Main.Write("disk initialized");
        } else {
            // Restoring actions
            boolean failed = false;
            Stack<String> lines = new Stack<>();
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line = null;
                while ((line = reader.readLine()) != null)
                    lines.push(line.trim());
                reader.close();

                // Reverse the stack
                Stack<String> linesTemp = new Stack<>();
                while (!lines.isEmpty())
                    linesTemp.push(lines.pop());
                lines = linesTemp;

                // put bitmap information
                disk.meta().setBitmask(Long.parseLong(lines.pop()));

                // put descriptor information
                for (int i = 0; i < 24; i++) {
                    disk.meta().getDescriptor(i).setContents(Long.parseLong(lines.pop()));
                }
                disk.meta().calculateUnusedDescriptors();

                // put block information
                int block_index = 0;
                while(!lines.isEmpty()) {
                    Block block = new Block();
                    for (int i = 0; i < 64; i++) {
                        block.set(i, (char) Integer.valueOf(lines.pop()).byteValue());
                    }
                    disk.write_block(block_index, block);
                    block_index++;
                }
                oft.setBuffer(0, disk.read_block(0));
            } catch (FileNotFoundException e) {
                failed = true;
            } catch (IOException e) {
                failed = true;
            }

            if (!failed)
                Main.Write("disk restored");
            else {
                reset();
                Main.Write("failed to open file. Initializing Disk Instead.");
            }
        }
    }

    /**
     * Save the logical disk to the default file.
     *
     * @return the path of the file where the disk was saved.
     */
    public String save() {
        return save(DEFAULT_SAVE_FILE);
    }

    /**
     * Save the logical disk to the specified file.
     *
     * @param file_name
     * @return the path of the file where the disk was saved.
     */
    public String save(String file_name) {
        Main.Log("Saving disk with the \"" + file_name + "\" file.");

        for (int i : oft.getOpenIndecies())
            close(i, false);

        String diskInformation = disk.toString();

        try {
            Writer writer = new PrintWriter(file_name);
            writer.write(diskInformation);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Main.Write("disk saved");
        return null;
    }

    public static final String DEFAULT_SAVE_FILE = "file.txt";

    private class DirectoryEntry {
        private String mName;
        private int mIndex;

        public DirectoryEntry(int index, String name) {
            mIndex = index;
            mName = name.substring(0, 4);
        }
    }

    private boolean allZero(char[] ts) {
        for (char t : ts) {
            if (t != 0) {
                return false;
            }
        }
        return true;
    }

    private char[] trimZero(char[] cs) {
        int i = 1;
        while (i <= cs.length && cs[i - 1] != 0)
            i++;
        return Arrays.copyOf(cs, i);
    }

    /**
     * Inteface defines the actions that we should expect the disk to be able to do.
     */
    public interface DiskInterface {
        public Block read_block(int i);

        public void write_block(int i, Block value);

        public Meta meta();

    }

    private String[] trimNull(String[] strs) {
        String[] result = new String[strs.length];
        int i = 0;
        for (String s : strs) {
            result[i] = s.trim();
            i++;
        }
        return result;
    }
}
