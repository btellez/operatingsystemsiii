package os.components;

public class Command {
    public static enum Type { CR, CD, DE, OP, CL, RD, WR, SK, DR, IN, SV};

    public class Args {
        public int count;
        public String arg1;
        public String arg2;
        public String arg3;
    }

    private Type mCommand;
    private Args mArguments;

    public Command(String[] rawCommand) throws Exception {
        parse(rawCommand);
    }

    public Type getCommand() {
        return mCommand;
    }

    public Args getArgs() {
        return mArguments;
    }

    public boolean expectedArguments(int count) throws InvalidCommandException {
        if (mArguments.count == count)
            return true;
        throw new InvalidCommandException("Invalid Number of Arguments Provided");
    }

    private void parse(String[] rawCommand) throws Exception {
        try {
            // Get the Type via the valueOf function. Assumtion that all commands are syntactically correct.
            mCommand = Type.valueOf(rawCommand[0].toUpperCase());
            mArguments = new Args();
            mArguments.count = rawCommand.length - 1;

            switch (rawCommand.length) {
                case 4:
                    mArguments.arg3 = rawCommand[3];
                case 3:
                    mArguments.arg2 = rawCommand[2];
                case 2:
                    mArguments.arg1 = rawCommand[1];
                    break;
            }
        } catch(Exception e) {
            throw new Exception("Invalid Command Given.");
        }
    }

    public static class InvalidCommandException extends Exception {
        public InvalidCommandException(String str) {
            super(str);
        }
    }
}
