package os;

import os.components.Command;
import os.components.FileSystem;
import os.components.LogicalDisk;

import java.util.Scanner;

public class Main {
    private static Scanner stream = new Scanner(System.in);
    private static FileSystem fs = new FileSystem();

    public static boolean LOGGING = true;

	public static void main(String[] args) {
//		Main.Write("CS 143B - Project Project 3");
//        Main.Write("Bladymir Tellez (#76123163) + Calvin Liu (#)");
        LOGGING = false;
        while (stream.hasNext())
            execute(stream.nextLine().trim());
	}

    /**
     * Execute the given command.
     * @param command
     */
    public static void execute(String command) {
        if (command.equals("")) return;
        try {
            processCommand(command);
        } catch (Command.InvalidCommandException e) {
            System.out.println("Error: Bad Command -- Ignoring last command.");
        } catch (UnsupportedOperationException e) {
            System.out.println("Error: Init Does Not Support this operation.");
        } catch (Exception e) {
            System.out.println("Error: " +e.getMessage());
        }
    }

    /**
     * Process a single command.
     * @param commandString
     * @return
     * @throws Command.InvalidCommandException
     */
    public static void processCommand(String commandString) throws Command.InvalidCommandException, Exception{
        String[] parts = commandString.split(" ");
        Command command = new Command(parts);

        switch (command.getCommand()) {
            case CR:
            case CD: // <name>
                command.expectedArguments(1);
                fs.create(command.getArgs().arg1);
                break;

            case DE: // <name>
                command.expectedArguments(1);
                fs.destroy(command.getArgs().arg1);
                break;

            case OP: // <name>
                command.expectedArguments(1);
                fs.open(command.getArgs().arg1);
                break;

            case CL: // <index>
                command.expectedArguments(1);
                fs.close(toint(command.getArgs().arg1));
                break;

            case RD: // <index> <count>
                command.expectedArguments(2);
                fs.read(toint(command.getArgs().arg1), toint(command.getArgs().arg2));
                break;

            case WR: // <index> <char> <count>
                command.expectedArguments(3);
                Command.Args args = command.getArgs();
                fs.write(toint(args.arg1), tochar(args.arg2), toint(args.arg3));
                break;

            case SK: // <index> <pos>
                command.expectedArguments(2);
                fs.seek(toint(command.getArgs().arg1), toint(command.getArgs().arg2));
                break;

            case DR: //
                command.expectedArguments(0);
                fs.directory(true);
                break;

            case IN: // <disk cont.txt>
                if (command.getArgs().count > 0)
                    fs.init(command.getArgs().arg1);
                else
                    fs.init();
                break;
				
            case SV: // <disk cont.txt>
                command.expectedArguments(1);
                fs.save(command.getArgs().arg1);
                break;

            default:
                System.out.println("Process terminated");
                return; // Quit the application.
        }
    }

    public static int toint(String s) throws Command.InvalidCommandException {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            throw new Command.InvalidCommandException("Error: Expecting Integer Parameter.");
        }
    }

    public static char tochar(String s) throws Command.InvalidCommandException {
        try {
            if (s.length() > 1)
                throw new Exception();
            return s.charAt(0);
        } catch (Exception e) {
            throw new Command.InvalidCommandException("Error: Expecting Single Character Parameter");
        }
    }

    public static void Write(String... message) {
        for (String s : message)
            System.out.println(s);
    }

    public static void Log(String... message) {
        if (!LOGGING) return;
        for (String s : message) {
            System.out.println(s);
        }
    }
}

